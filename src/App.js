import React from 'react';
import { Switch, Router, Route } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';

import './App.css'
import Home from './components/Home';
import  CountriesList  from './components/CountriesList';
import  ContinentsList  from './components/ContinentsList';
import CountryItem from './components/CountryItem';
import  PageNotFound  from './components/PageNotFound';
import { API } from './config';
import ContinentItem from './components/ContinentItem';
import { AppWrapper } from './components/UIComponents';


const customHistory = createBrowserHistory()


const client = new ApolloClient({
  uri: API.URL
}
);

function App() {
  return (
    <div className="App">
      <ApolloProvider client={client}>
        <AppWrapper>
          <Router history={customHistory}>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/continents" component={ContinentsList} />
              <Route exact path="/countries" component={CountriesList} />
              <Route path="/countries/:countryId" component={CountryItem} />
              <Route path="/continents/:continentId" component={ContinentItem} />
              <Route path="*" component={PageNotFound} />
            </Switch>
          </Router>
        </AppWrapper>
      </ApolloProvider>
    </div>
  );
}

export default App;
