import React from 'react';
 
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { Loader } from './Loader';
import { CountryCard } from './CountryCard';
import { PageWrapper } from './UIComponents';
import withNav from './withNav';
 
const GET_COUNTRIES = gql`
  query{
    countries {
      name
      native
      phone
      code
      emoji
      languages{
        name
        native
      }
      continent{
        name
        code
      }
    }
  }
`

const CountriesList = () => {
    const { loading, error, data } = useQuery(GET_COUNTRIES);
    if (loading) return <Loader />;
    if (error) return <p>ERROR:</p>;
    const { countries } = data
    return (<PageWrapper>
        <h1>Countries:  </h1>
        <div>
            {countries.map((country) => (
                <CountryCard {...country} />))
            }
        </div>
    </PageWrapper>
    )
}

export default withNav(CountriesList)