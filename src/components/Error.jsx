import React from 'react';
import withNav from './withNav';

 const Error = () =>{
    return (<div>Something went wrong :(</div>)
}

export default withNav(Error)