import React from 'react';
import withNav from './withNav';

export const PageNotFound = () =>{
    return (<div>Page not found :(</div>)
}

export default withNav(PageNotFound)