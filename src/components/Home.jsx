import React from 'react';
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { ROUTS } from '../config';
import { PageWrapper } from './UIComponents';
import withNav from './withNav';


const StyledLink = styled(Link)`
 display:flex;
 justify-content:center;
 align-items:center;
 flex:1;
 flex-direction:row;
 font-size:18px;
`
const Home = () => {
    return (
        <PageWrapper>
            <StyledLink to={ROUTS.COUNTRIES}> <h1>Countries</h1> </StyledLink>
            <StyledLink to={ROUTS.CONTINENTS}> <h1>Continents</h1> </StyledLink>
        </PageWrapper>
    )
}

export default withNav(Home)