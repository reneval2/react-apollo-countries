import React from 'react';
import { Link } from 'react-router-dom'
import { ContinentLink } from './ContinentLink';
import { ListItem } from './UIComponents';
import styled from 'styled-components'

const Wrapper = styled.div`
 text-align:center;
 align-items:center;
 justify-content:center;
padding: 10px;
 box-shadow: 2px 0px 12px rgba(0, 0, 0, 0.07);
 & ul{
   margin-top:0;
   color:#A6A6A6;
   list-style:none;
 }
`
const Header = styled.div`
 align-items:center;
 justify-content:center;
 display:flex;
 flex-direction:row;
`
const StyledLink = styled(Link)`
 display:flex;
 flex-direction:row;
 font-size:18px;
`

export const CountryCard = ({ name, code, languages, continent, emoji }) => {
  return (
    <ListItem>
      <Wrapper>
        <Header>
          <StyledLink to={`/countries/${code}`}>
            <p>{emoji}</p>&nbsp;<p> {name} / </p>
            <p><ContinentLink {...continent} /></p>
          </StyledLink>
        </Header>
        <ul>
          {
            languages.map(({ name, native }) => (
              <li>{`${name} - ${native}`}</li>
            ))
          }
        </ul>
      </Wrapper>
    </ListItem>
  )
}

 