import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { Link } from 'react-router-dom'
import { Loader } from './Loader';
import { ROUTS } from '../config';
import { List, ListItem } from './UIComponents';
import withNav from './withNav';
import Error from './Error';

const GET_CONTINENTS = gql`
  query{
    continents {
      name 
      code
    }
  }
`

const ContinentsList = () => {
  const { loading, error, data } = useQuery(GET_CONTINENTS);
  if (loading) return <Loader />;
  if (error) return <Error />;
  const { continents } = data
  return (<div>
    <h1>Continents: </h1>
    <List>
      {continents.map((continent) => (
        <ListItem>
          <Link to={`${ROUTS.CONTINENTS}/${continent.code}`}>
            <h3>{continent.name}</h3>
          </Link>
        </ListItem>
      ))}
    </List>
  </div>
  )
}

export default withNav(ContinentsList)