import styled from 'styled-components'

export const PageWrapper = styled.div`
display:flex;
flex-direction:row;
width:100%;
min-height:500px;
justify-content:center;
align-items:center;
`

export const AppWrapper = styled.div`
display:flex;
flex-direction:row;
width:100%;
justify-content:center;
align-items:center;

& div{
    width:100%;
}
`

export const List = styled.ul`
list-style: none 
`

export const ListItem = styled.li`
font-size: 16px;
list-style: none 
`
 
 