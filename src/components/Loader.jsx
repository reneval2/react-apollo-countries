import React from 'react';
import styled from 'styled-components'
import './Loader.css'

const Wrapper = styled.div`
 display:flex;
 justify-content:center;
 align-items:center;
 flex:1;
    position: fixed;
    left: 0;
    top: 0;
    height: 100%;
    align-items: center;
`
 


export const Loader = () => (
<Wrapper>
<div id="loading"></div>
</Wrapper>)
