import React from 'react'
import { withRouter } from 'react-router-dom'
import styled from 'styled-components'

const withNav = Component => withRouter(({history, ...props}) => {
 const Wrapper =  styled.div`
display:flex;
  position: absolute;
  top: 10px;
  left: 50px;
  justify-content:space-between;
  width:300px;
`
const BackLink= styled.button`
  background-color: transparent;
  border: none;
  cursor: pointer;
  text-decoration: underline;
  display: inline;
  margin: 0;
  padding: 0;
  height: 200px;
  outline:none;
`
 
  return (
  <Wrapper>
    <BackLink onClick={()=>history.push("/")}>Home</BackLink> &nbsp;&nbsp;
    <BackLink onClick={()=>history.goBack()}>Back</BackLink>
    <Component {...props} /> 
    </Wrapper>
    )
}
)
 
export default withNav
 
