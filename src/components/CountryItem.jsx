import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { Loader } from './Loader';
import withNav from './withNav';
import Error from './Error';

const GET_COUNTRY_BY_CODE = gql`
  query getCountty($code: String!) {
    country(code: $code) {
      name
      phone
      code
      emoji
      currency
    }
    }
` 

const CountryItem = ({ match, ...rest }) => {
    const { params: { countryId } } = match
    const { loading, error, data } = useQuery(GET_COUNTRY_BY_CODE, {
        variables: { code: countryId },
    });
    if (loading) return <Loader />;
    if (error) return <Error />
    const { country: {name,emoji,phone,currency} } = data
    return (<div>
        <h1>{emoji} {name}</h1>
        <h3>Code: {phone}</h3>
        <h3>Currency: {currency}</h3>
    </div>
    )
}

export default withNav(CountryItem)