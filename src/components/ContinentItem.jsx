import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { Link } from 'react-router-dom'
import gql from 'graphql-tag';
import { Loader } from './Loader';
import { List, ListItem } from './UIComponents';
import withNav from './withNav';
import Error from './Error';

const GET_CONTINENT_BY_CODE = gql`
  query getContinent($code: String!) {
    continent(code: $code) {
      name
      code
    countries{
      name
      code
      currency
      emoji
    }
    }
  }
`
 const ContinentItem = ({ match, ...rest }) => {
  const { params: { continentId } } = match
  const { loading, error, data } = useQuery(GET_CONTINENT_BY_CODE, {
    variables: { code: continentId },
  });
  if (loading) return <Loader />
  if (error) return <Error />
  const { continent } = data
  const { countries, name, code } = continent
  return (<div>
    <h1>Continent: {`${name}, ${code}`}</h1>
    <List divided relaxed horizontal>
      {countries.map((country) => (
        <ListItem key={country.code}>
          <Link to={`/countries/${country.code}`}>
            <p><span>{country.emoji}</span>{country.name}</p>
          </Link>
        </ListItem>
      ))}
    </List>
  </div >
  )
}
 
export default withNav(ContinentItem)