import React from 'react';
import { Link } from 'react-router-dom'
import { ROUTS } from '../config';

export const ContinentLink = ({ name, code }) => {
    return (
        <Link to={`${ROUTS.CONTINENTS}/${code}`}>
            <div> {name}</div>
        </Link>
    )
}