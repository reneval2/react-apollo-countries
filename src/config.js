export const API = {
    URL: 'https://countries.trevorblades.com/'
}

export const ROUTS = {
    COUNTRIES: '/countries',
    CONTINENTS: '/continents',
}